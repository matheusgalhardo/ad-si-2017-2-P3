Introdução

Olá !

Seja bem-vindo ao **Projeto 3** da matéria Aplicações Distribuídas!

Disciplina ministrada pelo **Professor Me. Marcelo Akira**!

Aluno responsável:

- @matheusgalhardo :writing_hand:

Acesse a documentação completa na [Wiki](https://gitlab.com/matheusgalhardo/ad-si-2017-2-P3/wikis/home) do nosso projeto.


